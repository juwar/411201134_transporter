<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use App\Models\Pengiriman;
use App\Models\Lokasi;
use App\Models\Barang;
use App\Models\Kurir;
use DateTime;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = app('App\Http\Controllers\API\PengirimanController')->index()->getData()->data;
        Log::info('transaksi', [$transaksi]);
        return view('transaksi.index', compact('transaksi'))->with('active_sidebar', 'transaksi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_barang = Barang::where('deleted_at', '=', null)->select('nama_barang', 'id')->get();
        $list_lokasi = Lokasi::where('deleted_at', '=', null)->select('nama_lokasi', 'id')->get();
        return view('transaksi.create', compact('list_barang', 'list_lokasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $responses = app('App\Http\Controllers\API\PengirimanController')->store($request)->getData();
        Log::info('responses', [$responses]);
        return redirect('transaksi')->with('responses', $responses)->with('active_sidebar', 'transaksi');
    }

    /**
     * Approve.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'kurir_id' => 'required',
            'no_pengiriman' => 'required',
        ]);

        //Check Pengiriman
        $pengiriman = Pengiriman::select('id')
            ->where('no_pengiriman', '=', $request->input('no_pengiriman'))
            ->get();

        //Check Kurir
        $kurir = Kurir::select('id', 'name', 'email')
            ->where('id', '=', $request->input('kurir_id'))
            ->whereNull('deleted_at')
            ->get();

        Log::info('Pengiriman', [json_encode($pengiriman)]);
        Log::info('Kurir', [json_encode($kurir)]);

        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }
        if (count($pengiriman) < 1) {
            return response()->json(['message' => 'error', 'data' => 'Data Pengiriman tidak ditemukan'], 400);
        }
        if (count($kurir) < 1) {
            return response()->json(['message' => 'error', 'data' => 'Kurir tidak ditemukan'], 400);
        }

        $now = new DateTime();

        #QUERY BUILDER
        DB::table('pengiriman')->where('id', $pengiriman[0]['id'])->update([
            'kurir_id' => $request->input('kurir_id'),
            'approved_at' => $now,
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}