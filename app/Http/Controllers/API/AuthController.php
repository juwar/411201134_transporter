<?php

namespace App\Http\Controllers\API;

use App\Models\Kurir;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:kurir'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }

        $kurir = Kurir::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'api_token' => Str::random(60)
        ]);

        return response()->json(['message' => 'berhasil create', 'data' => $kurir], 200);
    }


}