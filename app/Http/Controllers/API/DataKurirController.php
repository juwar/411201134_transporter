<?php

namespace App\Http\Controllers\API;

use App\Models\Kurir;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DateTime;

class DataKurirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listKurir = Kurir::select('id', 'name', 'email')
            ->where('deleted_at', '=', null)
            ->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $listKurir], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }

        #QUERY BUILDER
        DB::table('kurir')->insert([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Kurir::select('id', 'name', 'email')
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $detail], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }

        #QUERY BUILDER
        DB::table('kurir')->where('id', $id)->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil diupdate'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $now = new DateTime();

        #QUERY BUILDER
        DB::table('kurir')->where('id', $id)->update([
            'deleted_at' => $now,
        ]);

        return response()->json(['message' => 'delete berhasil', 'data' => 'berhasil dihapus'], 201);
    }
}