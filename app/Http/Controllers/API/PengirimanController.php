<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengiriman;
use App\Models\Lokasi;
use App\Models\Barang;
use App\Models\Kurir;
use DateTime;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listPengiriman = DB::table('pengiriman')
            ->join('kurir', 'pengiriman.kurir_id', '=', 'kurir.id')
            ->join('barang', 'pengiriman.barang_id', '=', 'barang.id')
            ->join('lokasi', 'pengiriman.lokasi_id', '=', 'lokasi.id')
            ->select('pengiriman.*', 'kurir.name', 'barang.nama_barang', 'lokasi.nama_lokasi')->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $listPengiriman], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'no_pengiriman' => 'required',
            // 'tanggal' => 'required',
            'lokasi_id' => 'required',
            'barang_id' => 'required',
            'jumlah_barang' => 'required',
        ]);

        //Check Lokasi
        $lokasi = Lokasi::select('id', 'kode_lokasi', 'nama_lokasi')
            ->where('id', '=', $request->input('lokasi_id'))
            ->whereNull('deleted_at')
            ->get();

        //Check Barang
        $barang = Barang::select('id', 'kode_barang', 'nama_barang', 'deskripsi', 'stok_barang', 'harga_barang')
            ->where('id', '=', $request->input('barang_id'))
            ->whereNull('deleted_at')
            ->get();

        $harga = $request->input('jumlah_barang') * $barang[0]['harga_barang'];

        Log::info('Lokasi', [json_encode($lokasi)]);
        Log::info('Barang', [json_encode($barang)]);
        Log::info('Harga', [$harga]);

        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }
        if (count($lokasi) < 1) {
            return response()->json(['message' => 'error', 'data' => 'Lokasi tidak ditemukan'], 400);
        }
        if (count($barang) < 1) {
            return response()->json(['message' => 'error', 'data' => 'Barang tidak ditemukan'], 400);
        }
        if ($request->input('jumlah_barang') > $barang[0]['stok_barang']) {
            return response()->json(['message' => 'error', 'data' => 'Stok barang tidak cukup'], 400);
        }


        #QUERY BUILDER
        DB::table('pengiriman')->insert([
            'no_pengiriman' => $request->input('no_pengiriman'),
            'tanggal' => date("Y-m-d"),
            'lokasi_id' => $request->input('lokasi_id'),
            'barang_id' => $request->input('barang_id'),
            'jumlah_barang' => $request->input('jumlah_barang'),
            'harga_barang' => $harga,
            'kurir_id' => Auth::user()->id,
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Approve.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'kurir_id' => 'required',
            'no_pengiriman' => 'required',
        ]);

        //Check Pengiriman
        $pengiriman = Pengiriman::select('id')
            ->where('no_pengiriman', '=', $request->input('no_pengiriman'))
            ->get();

        //Check Kurir
        $kurir = Kurir::select('id', 'name', 'email')
            ->where('id', '=', $request->input('kurir_id'))
            ->whereNull('deleted_at')
            ->get();

        Log::info('Pengiriman', [json_encode($pengiriman)]);
        Log::info('Kurir', [json_encode($kurir)]);

        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }
        if (count($pengiriman) < 1) {
            return response()->json(['message' => 'error', 'data' => 'Data Pengiriman tidak ditemukan'], 400);
        }
        if (count($kurir) < 1) {
            return response()->json(['message' => 'error', 'data' => 'Kurir tidak ditemukan'], 400);
        }

        $now = new DateTime();

        #QUERY BUILDER
        DB::table('pengiriman')->where('id', $pengiriman[0]['id'])->update([
            'kurir_id' => $request->input('kurir_id'),
            'approved_at' => $now,
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil disubmit'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}