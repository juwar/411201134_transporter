<?php

namespace App\Http\Controllers\API;

use App\Models\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DateTime;

class DataBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listBarang = Barang::select('id', 'kode_barang', 'nama_barang', 'deskripsi', 'stok_barang', 'harga_barang')
            ->where('deleted_at', '=', null)
            ->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $listBarang], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required|unique:barang,kode_barang',
            'nama_barang' => 'required'
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }

        #ELOQUENT
        /* $barang = new Barang();
        $barang->code = $request->input('code');
        $barang->name = $request->input('name');
        $barang->save(); */

        #QUERY BUILDER
        DB::table('barang')->insert([
            'kode_barang' => $request->input('kode_barang'),
            'nama_barang' => $request->input('nama_barang'),
            'deskripsi' => $request->input('deskripsi'),
            'stok_barang' => $request->input('stok_barang'),
            'harga_barang' => $request->input('harga_barang'),
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil disubmit'], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Barang::select('id', 'kode_barang', 'nama_barang', 'deskripsi', 'stok_barang', 'harga_barang')
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        return response()->json(['message' => 'Data tersedia', 'data' => $detail], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required',
            'nama_barang' => 'required'
        ]);
        #RETURN VALIDATOR
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages], 400);
        }

        #QUERY BUILDER
        DB::table('barang')->where('id', $id)->update([
            'kode_barang' => $request->input('kode_barang'),
            'nama_barang' => $request->input('nama_barang'),
            'deskripsi' => $request->input('deskripsi'),
            'stok_barang' => $request->input('stok_barang'),
            'harga_barang' => $request->input('harga_barang'),
        ]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil diupdate'], 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $now = new DateTime();

        #QUERY BUILDER
        DB::table('barang')->where('id', $id)->update([
            'deleted_at' => $now,
        ]);

        return response()->json(['message' => 'delete berhasil', 'data' => 'berhasil dihapus'], 201);
    }
}