<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('/admin');
});

Route::get('/admin', function () {
    return view('admin.dashboard')->with('active_sidebar', 'dashboard');
});


// Route::get('/', 'Web\BarangController@index');

Route::get('/admin', 'AdminController@index');
Route::get('/admin', 'AdminController@index');
Route::get('/transaksi', 'Web\PengirimanController@index');
Route::get('/transaksi-create', 'Web\PengirimanController@create');
Route::post('/transaksi-create-process', 'Web\PengirimanController@store');