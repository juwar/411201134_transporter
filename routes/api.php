<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\AuthController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('list-barang', 'API\DataBarangController@index');
    Route::post('add-barang', 'API\DataBarangController@store');
    Route::get('find-barang/{id}', 'API\DataBarangController@show');
    Route::post('update-barang/{id}', 'API\DataBarangController@update');
    Route::post('delete-barang/{id}', 'API\DataBarangController@destroy');

    Route::get('list-kurir', 'API\DataKurirController@index');
    Route::post('add-kurir', 'API\DataKurirController@store');
    Route::get('find-kurir/{id}', 'API\DataKurirController@show');
    Route::post('update-kurir/{id}', 'API\DataKurirController@update');
    Route::post('delete-kurir/{id}', 'API\DataKurirController@destroy');

    Route::get('list-lokasi', 'API\DataLokasiController@index');
    Route::post('add-lokasi', 'API\DataLokasiController@store');
    Route::get('find-lokasi/{id}', 'API\DataLokasiController@show');
    Route::post('update-lokasi/{id}', 'API\DataLokasiController@update');
    Route::post('delete-lokasi/{id}', 'API\DataLokasiController@destroy');

    Route::post('create-transaksi', 'API\PengirimanController@store');
    Route::post('approve', 'API\PengirimanController@approve');
});