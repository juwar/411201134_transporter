@extends('layouts.master')

@section('header')
    <div class="panel-header panel-header-lg">
        <canvas id="bigDashboardChart"></canvas>
    </div>
@endsection

@section('sidebar')
    {{ $active = isset($active_sidebar) ? $active_sidebar : '' }}
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $active == 'dashboard' ? 'active' : '' }}">
                <a href="./admin">
                    <i class="now-ui-icons design_app"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{{ $active == 'transaksi' ? 'active' : '' }}">
                <a href="./transaksi">
                    <i class="now-ui-icons education_atom"></i>
                    <p>Transaksi</p>
                </a>
            </li>
        </ul>
    </div>
@endsection
