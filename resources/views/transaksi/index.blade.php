@extends('layouts.home')

@section('title')
    Transaksi
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row col-md-12 justify-content-md-between">
                        <h4 class="card-title"> Transaksi</h4>
                        <a href="/transaksi-create" class="btn btn-primary" href="#">
                            {{ __('Create') }}
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    No Pengiriman
                                </th>
                                <th>
                                    Tanggal
                                </th>
                                <th>
                                    Lokasi
                                </th>
                                <th>
                                    Nama Barang
                                </th>
                                <th>
                                    Jumlah Barang
                                </th>
                                <th>
                                    Harga Barang
                                </th>
                                <th>
                                    Kurir
                                </th>
                            </thead>
                            <tbody>
                                @foreach ($transaksi as $item)
                                    <tr>
                                        <td>
                                            {{ $item->no_pengiriman }}
                                        </td>
                                        <td>
                                            {{ $item->tanggal }}
                                        </td>
                                        <td>
                                            {{ $item->nama_lokasi }}
                                        </td>
                                        <td>
                                            {{ $item->nama_barang }}
                                        </td>
                                        <td>
                                            {{ $item->jumlah_barang }}
                                        </td>
                                        <td>
                                            {{ $item->harga_barang }}
                                        </td>
                                        <td>
                                            {{ $item->name }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
