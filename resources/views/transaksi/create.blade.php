@extends('layouts.home')

@section('title')
    Transaksi
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="/transaksi-create-process">
                        @csrf

                        <div class="form-group row">
                            <label for="no_pengiriman"
                                class="col-md-4 col-form-label text-md-right">{{ __('No pengiriman') }}</label>

                            <div class="col-md-6">
                                <input id="no_pengiriman" type="text"
                                    class="form-control @error('no_pengiriman') is-invalid @enderror" name="no_pengiriman"
                                    value="{{ old('no_pengiriman') }}" required autocomplete="no_pengiriman" autofocus>

                                @error('no_pengiriman')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="barang_id"
                                class="col-md-4 col-form-label text-md-right">{{ __('Nama Barang') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select form-control @error('barang_id') is-invalid @enderror"
                                    name="barang_id" id="barang_id">
                                    <option selected>Choose...</option>
                                    @foreach ($list_barang as $item)
                                        <option value={{ $item->id }}>{{ $item->nama_barang }}</option>
                                    @endforeach
                                </select>

                                @error('barang_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lokasi_id" class="col-md-4 col-form-label text-md-right">{{ __('Lokasi') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select form-control @error('lokasi_id') is-invalid @enderror"
                                    name="lokasi_id" id="lokasi_id">
                                    <option selected>Choose...</option>
                                    @foreach ($list_lokasi as $item)
                                        <option value={{ $item->id }}>{{ $item->nama_lokasi }}</option>
                                    @endforeach
                                </select>

                                @error('lokasi_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jumlah_barang"
                                class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Barang') }}</label>

                            <div class="col-md-6">
                                <input id="jumlah_barang" type="number"
                                    class="form-control @error('jumlah_barang') is-invalid @enderror" name="jumlah_barang"
                                    value="{{ old('jumlah_barang') }}" required autocomplete="jumlah_barang" autofocus>

                                @error('jumlah_barang')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Tambah') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
