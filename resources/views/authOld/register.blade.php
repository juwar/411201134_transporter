@extends('layouts.master')

@section('title')
    Register
@endsection

@section('header')
    <div class="panel-header panel-header-sm">
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">Register</h4>
          </div>
          <div class="card-body">
                <form>
                    <div class="form-group">
                    <label for="inputName">Username</label>
                    <input type="text" class="form-control" id="inputName" aria-describedby="nameHelp" placeholder="Enter your name">
                    <label for="inputEmail">Email address</label>
                    <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection